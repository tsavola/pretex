PYTHON		:= python
RUBBER		:= rubber
DVIPDF		:= dvipdf
DIA		:= dia

PRETEX_FILES	:= $(wildcard *.pretex)
TEX_FILES	:= $(PRETEX_FILES:%.pretex=build/%.tex)
DVI_FILES	:= $(TEX_FILES:.tex=-slides.dvi) $(TEX_FILES:.tex=-handout.dvi)
PDF_SLIDES	:= $(PRETEX_FILES:%.pretex=%.pdf)
PDF_HANDOUTS	:= $(PRETEX_FILES:%.pretex=handouts/%.pdf)
PDF_ALL		:= $(PDF_SLIDES) $(PDF_HANDOUTS)

DIA_FILES	:= $(wildcard */*.dia)
EPS_FILES	:= $(DIA_FILES:%.dia=build/%.eps)

define echo
	@ printf "  %-4s %s\n" "$1" "$2"
endef

slides: $(PDF_SLIDES)
handouts: $(PDF_HANDOUTS)
all: slides handouts

$(PDF_ALL): $(EPS_FILES)
$(DVI_FILES): $(EPS_FILES)

%.pdf: build/%-slides.dvi
	$(call echo,DVI,$@)
	@ cd build && $(DVIPDF) $*-slides.dvi ../$*.pdf

handouts/%.pdf: build/%-handout.dvi
	$(call echo,DVI,$@)
	@ mkdir -p handouts
	@ cd build && $(DVIPDF) $*-handout.dvi ../handouts/$*.pdf

# Disable implicit rule
%.dvi: %.tex

build/%-slides.dvi: build/%.tex
	$(call echo,TEX,$@)
	@ echo > build/$*-slides.tex \
		"\\documentclass{beamer}" \
		"\\newcommand{\\pagelayout}{}" \
		"\\input{$*.tex}"
	@ $(RUBBER) -q --into build build/$*-slides.tex

build/%-handout.dvi: build/%.tex
	$(call echo,TEX,$@)
	@ echo > build/$*-handout.tex \
		"\\documentclass[handout]{beamer}" \
		"\\usepackage{pgfpages}" \
		"\\newcommand{\\pagelayout}{\\pgfpagesuselayout{4 on 1}}" \
		"\\input{$*.tex}"
	@ $(RUBBER) -q --into build build/$*-handout.tex

build/%.tex: %.pretex
	$(call echo,PRE,$@)
	@ mkdir -p $(dir $@)
	@ $(PYTHON) preprocess.py $*.pretex $@

build/%.eps: %.dia
	$(call echo,DIA,$@)
	@ mkdir -p $(dir $@)
	@ $(DIA) -t eps -e $@ $*.dia

clean:
	rm -rf build
	rm -f $(PDF_ALL)
	rmdir handouts

.PRECIOUS: $(TEX_FILES)

.PHONY: slides handouts all clean
