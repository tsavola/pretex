import re

def main(input, output):
	level_info = [(0, None)]
	inside_frame = False

	for line in input:
		index = 0
		ignore = False
		for c in line:
			if not ignore:
				if c == '\\':
					ignore = True
				elif c == '%':
					break
			index += 1

		while index > 0 and line[index - 1].isspace():
			index -= 1

		content = line[:index]
		if not content.strip():
			print >>output, content
			continue

		match = re.match(r'^(\s*)([^\s].*)', content)
		curr_indent = len(match.group(1).replace('\t', ' ' * 8))

		match = re.match(r'^([\*\-])(.*)', match.group(2))
		if match:
			if match.group(1) == '-':
				curr_type = 'itemize'
			else:
				curr_type = 'enumerate'

			content = match.group(2)
		else:
			curr_type = None

		while True:
			prev_indent, prev_type = level_info[-1]
			if curr_indent >= prev_indent and \
			   not (curr_indent == prev_indent and curr_type != prev_type):
				break
			if prev_type:
				spaces = ' ' * prev_indent
				print >>output, spaces + r'\end{%s}' % prev_type
			level_info.pop()

		if curr_indent > prev_indent:
			level_info.append((curr_indent, curr_type))

		if curr_type:
			spaces = ' ' * curr_indent

			if curr_indent > prev_indent or \
			   (curr_indent == prev_indent and curr_type != prev_type):
				print >>output, spaces + r'\begin{%s}' % curr_type

			print >>output, spaces + r'\item' + content
		else:
			if re.match(r'^==+\s*$', content):
				print >>output, '}'

				inside_frame = False
				continue

			match = re.match(r'^==+\s+([^=]*)', content)
			if match:
				if inside_frame:
					print >>output, '}'

				title = match.group(1).strip()
				print >>output, r'\frame{\frametitle{%s}' % title

				inside_frame = True
				continue

			print >>output, content

	output.close()
	input.close()

if __name__ == '__main__':
	import sys, os

	input = open(sys.argv[1], 'r')
	output = open(sys.argv[2], 'w')

	try:
		main(input, output)
	except:
		if sys.argv[2] != '/dev/stdout':
			os.unlink(sys.argv[2])
		raise
